package hr.riteh.akeyboard;

/**
 * Created by Antonio on 17.2.2015..
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {
    //database version
    private static final int DATABASE_VERSION = 1;
    //database name
    private static final String DATABASE_NAME = "akeyboard_new.db";
    //common columns
    public static final String KEY_ID ="_id";
    //table names
    public static final String TABLE_WORDS = "words";
    public static final String WORD_ID = "_id";
    public static final String WORD_WORD = "word";

    public static final String TABLE_CATEGORIES = "categories";
    public static final String CATEGORY_ID = "_id";
    public static final String CATEGORY_NAME = "name";

    public static final String TABLE_WORD_POSITIONS = "word_positions";
    public static final String WP_POSITION = "position";
    public static final String WP_CATEGORY_ID = "category_id";
    //sprema id buttona koji je vezan u gridu
    public static final String WP_WORD_ID = "word_id";

   /*// Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_WORDS + "(" + WP_WORD_ID
            + " integer primary key autoincrement, " + WORD_WORD
            + " text not null);";
    */
   //table creation sql statements
   //word
   private static final String CREATE_TABLE_WORD = "CREATE TABLE IF NOT EXISTS "
           + TABLE_WORDS + "(" + WORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +  WORD_WORD
           + " TEXT UNIQUE" + ")";

    // category table create statement
    private static final String CREATE_TABLE_CATEGORY = "CREATE TABLE IF NOT EXISTS " + TABLE_CATEGORIES
            + "(" + CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + CATEGORY_NAME + " TEXT UNIQUE"
            + ")";

    // word_category table create statement
    private static final String CREATE_TABLE_WORD_POSITIONS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_WORD_POSITIONS + "("
            + WP_CATEGORY_ID + " INTEGER," + WP_POSITION + " INTEGER," + WP_WORD_ID + " INTEGER" + ", FOREIGN KEY("+ WP_WORD_ID +
            ") REFERENCES "+TABLE_WORDS+"("+ WORD_ID +") ON DELETE CASCADE, " +
            " FOREIGN KEY("+ WP_CATEGORY_ID +") REFERENCES "+TABLE_CATEGORIES+"("+ CATEGORY_ID +") ON DELETE CASCADE," +
            " PRIMARY KEY(" + WP_CATEGORY_ID + "," + WP_POSITION +")"
            + ")";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE_CATEGORY);
        database.execSQL(CREATE_TABLE_WORD);
        database.execSQL(CREATE_TABLE_WORD_POSITIONS);
    }

    public void dropDatabase(){
        SQLiteDatabase database = this.getWritableDatabase();
        Log.d("DBINFO", "Dropping tables..");
        database.execSQL("DROP TABLE " + TABLE_CATEGORIES);
        database.execSQL("DROP TABLE " + TABLE_WORDS);
        database.execSQL("DROP TABLE " + TABLE_WORD_POSITIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORD_POSITIONS);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

}