package hr.riteh.akeyboard;

/**
 * Created by Antonio on 17.2.2015..
 */
public class Word {
    private long id;
    private String word;
    private int buttonId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String comment) {
        this.word = comment;
    }

    public int getButtonId() {
        return buttonId;
    }

    public void setButtonId(int buttonId) {
        this.buttonId = buttonId;
    }


    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return word;
    }
}