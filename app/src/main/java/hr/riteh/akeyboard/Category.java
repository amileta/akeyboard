package hr.riteh.akeyboard;

/**
 * Created by Antonio on 17.2.2015..
 */
public class Category {
        private long id;
        private String name;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        // Will be used by the ArrayAdapter in the ListView
        @Override
        public String toString() {
            return name;
        }
}