package hr.riteh.akeyboard;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class TestDatabaseActivity extends ListActivity {
    private WordsDataSource datasource;
    private CategoriesDataSource cat_datasource;
    private CategoriesWordsDataSource cw_datasource;

    public void setSelected(Category selected) {
        this.selected = selected;
    }

    private Category selected;
    private int selected_button_id;
    private int position=1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_database);

        datasource = new WordsDataSource(this);
        datasource.open();
        cat_datasource = new CategoriesDataSource(this);
        cat_datasource.open();
        cw_datasource = new CategoriesWordsDataSource(this);
        cw_datasource.open();

        List<Category> values = cat_datasource.getCategories();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this,
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);

        this.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Category item = (Category) getListView().getItemAtPosition(position);

                Toast.makeText(getBaseContext(), item.toString(), Toast.LENGTH_SHORT).show();
                //postavi oznacenu kategoriju
                selected = item;
                //loadaj nove rijeci za tu kategoriju
                //List words = cw_datasource.getWordsForCategory();

            }
        });
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Category> adapter = (ArrayAdapter<Category>) getListAdapter();
        Category category = new Category();
        EditText text = (EditText) findViewById(R.id.editText);
        //dodavanje kategorija
        switch (view.getId()) {
            case R.id.add:
                //String[] words = new String[] { "Cool", "Very nice", "Hate it" };
                //int nextInt = new Random().nextInt(3);
                category.setName(text.getText().toString());
                // save the new word to the database
                category.setId((int) cat_datasource.createCategory(category.getName()));
                if (category.getId() >= 0)
                    adapter.add(category);
                else
                    //handle duplicate
                Log.w("MyApp", "Adapter items:" + adapter.getCount());
                break;
            case R.id.delete:
                if (getListAdapter().getCount() > 0) {
                    category = (Category) getListAdapter().getItem(0);
                    cat_datasource.deleteCategory(category);
                    adapter.remove(category);
                }
                break;
            //dodavanje rijeci u odredjenu kategoriju
            case R.id.button36:
                Word word = new Word();
                word.setWord(text.getText().toString());
                long insertId = datasource.createWord(word);
                if (selected == null)
                    break;
                cw_datasource.addWordToCategory(selected.getId(), position, insertId);
                position++;
                Button button2 = (Button) findViewById(selected_button_id);
                button2.setText(word.getWord());
                //isprazni
                text.setText("");
                break;
            case R.id.grid9:
            case R.id.grid10:
            case R.id.grid11:
            case R.id.button32:
            case R.id.button33:
                selected_button_id = view.getId();
                Button button = (Button) findViewById(selected_button_id);
                if (button.getText().toString().isEmpty()) {
                    //prebaci na tekstbox
                    text.requestFocus();
                    //isprazni
                    text.setText("");
                }
                else {
                    //upisi rijec u editbox
                    text.append(" " + button.getText());
                }

        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        datasource.open();
        cw_datasource.open();
        cat_datasource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        cw_datasource.close();
        cat_datasource.close();
        super.onPause();
    }

}