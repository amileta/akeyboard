package hr.riteh.akeyboard;

/**
 * Created by Antonio on 17.2.2015..
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class WordsDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.WORD_ID,
            MySQLiteHelper.WORD_WORD};

    public WordsDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    //adds a word to category
    public long createWord(Word word) {
        long insertId;
        //check if the word is already added
        Cursor cursor = database.query(MySQLiteHelper.TABLE_WORDS,
                allColumns, MySQLiteHelper.WORD_ID + " = '" + word.getWord().toString() + "'", null, null, null, null);
        //if it's not added, add it:
        if (!cursor.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.WORD_WORD, word.getWord());
            //insert the word
            insertId = database.insert(MySQLiteHelper.TABLE_WORDS, null,
                    values);
            cursor.close();
            return insertId;
        }
        //else return the word id
        return cursor.getInt(0);
    }

    public void deleteWord(Word word) {
        long id = word.getId();
        System.out.println("Word deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_WORDS, MySQLiteHelper.WORD_ID
                + " = '" + id + "'", null);
    }

    //get a list of all the words
    public List<Word> getAllWords() {
        List<Word> words = new ArrayList<Word>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_WORDS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    private Word cursorToWord(Cursor cursor) {
        Word word = new Word();
        word.setId(cursor.getLong(0));
        word.setWord(cursor.getString(1));
        return word;
    }
}