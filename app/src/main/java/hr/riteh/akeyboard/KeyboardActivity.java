package hr.riteh.akeyboard;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Antonio on 18.2.2015..
 */
public class KeyboardActivity extends Activity {

    private WordsDataSource w_datasource;
    private CategoriesDataSource cat_datasource;
    private CategoriesWordsDataSource cw_datasource;
    private EditText text;
    //pohranjuje ako smo u keyboard ili category modu, default je category
    private int view_mode = 0;
    public KeyboardActivity() {
        super();
    }
    public static final int NUM_BUTTONS = 18;
    private Category selected_category;
    private int page_number;
    private List<Category> categories;
    private List<Word> current_words;
    private List<Button> gridSlots;
    //hashmapa fixiranih pozicija gumbi
    private static final Map<Integer, Integer> button_positions = new HashMap<Integer, Integer>();

    static {
        button_positions.put(R.id.grid1, 1);
        button_positions.put(R.id.grid2, 2);
        button_positions.put(R.id.grid3, 3);
        button_positions.put(R.id.grid4, 4);
        button_positions.put(R.id.grid5, 5);
        button_positions.put(R.id.grid6,6);
        button_positions.put(R.id.grid7,7);
        button_positions.put(R.id.grid8,8);
        button_positions.put(R.id.grid9,9);
        button_positions.put(R.id.grid10,10);
        button_positions.put(R.id.grid11,11);
        button_positions.put(R.id.grid12,12);
        button_positions.put(R.id.grid13,13);
        button_positions.put(R.id.grid14,14);
        button_positions.put(R.id.grid15,15);
        button_positions.put(R.id.grid16,16);
        //button_positions.put(R.id.grid17,17);
        //button_positions.put(R.id.grid18,18);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tipkovnica);
        w_datasource = new WordsDataSource(this);
        w_datasource.open();
        cat_datasource = new CategoriesDataSource(this);
        cat_datasource.open();
        cw_datasource = new CategoriesWordsDataSource(this);
        cw_datasource.open();

        categories = cat_datasource.getCategories();
        text = (EditText) findViewById(R.id.editText);
        TableLayout grid = (TableLayout) findViewById(R.id.grid);
        gridSlots = new ArrayList<Button>();
        //inicijalizacija kategorija
        TableLayout row = (TableLayout)((TableRow) grid.getChildAt(2)).getChildAt(0);
        for (int j = 0; j < 3; j++) {
            TableRow row_inner = (TableRow) row.getChildAt(j);
            for (int i = 0; i < row_inner.getChildCount(); i++) {
                if (row_inner.getChildAt(i) instanceof Button && row_inner.getChildAt(i).getId() != R.id.previous && row_inner.getChildAt(i).getId() != R.id.next) {
                    if ((j*NUM_BUTTONS + i) < categories.size()) {
                        gridSlots.add((Button) row_inner.getChildAt(i));
                        gridSlots.get(gridSlots.size() - 1).setText(categories.get(j*NUM_BUTTONS + i).getName());
                        gridSlots.get(gridSlots.size() - 1).setTag(categories.get(j*NUM_BUTTONS + i).getId());
                    } else
                        break;
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View view) {
        Button b;
        switch (view.getId()) {
            case R.id.editText:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                text.setFocusableInTouchMode(true);
                break;
            case R.id.category:
                //prebaci u grid s kategorijama
                fillGridWithCategories(0);
                //prebaci view mode
                view_mode = 0;
                break;
            case R.id.previous:
                if (view_mode == 0){
                    //popuni sa prethodnih xyz kategorija
                    fillGridWithCategories(page_number-1);
                }
                else{
                    fillGridWithWords(page_number-1);
                }
                break;
            case R.id.next:
                if (view_mode == 0){
                    //popuni sa sljedecih xyz kategorija
                    fillGridWithCategories(page_number+1);
                }
                else{
                    fillGridWithWords(page_number+1);
                }
                break;
            case R.id.speak:
                break;
            case R.id.home:
                finish();
                break;
            /*case R.id.prediction1:
            case R.id.prediction2:
            case R.id.prediction3:
            case R.id.prediction4:
            case R.id.prediction5:
                break;
            b = (Button)view;
            //upisi rijec u editbox
            text.append(b.getText());*/
            case R.id.grid1:
            case R.id.grid2:
            case R.id.grid3:
            case R.id.grid4:
            case R.id.grid5:
            case R.id.grid6:
            case R.id.grid7:
            case R.id.grid8:
            case R.id.grid9:
            case R.id.grid10:
            case R.id.grid11:
            case R.id.grid12:
            case R.id.grid13:
            case R.id.grid14:
            case R.id.grid15:
            case R.id.grid16:
            //ako smo u kategorijama
            if (view_mode == 0) {
                b = (Button)view;
                if (b.getText().toString().isEmpty())
                    break;
                //odaberi kategoriju
                selected_category = new Category();
                selected_category.setId(Long.parseLong(view.getTag().toString()));
                selected_category.setName(b.getText().toString());
                //dohvati rijeci iz te kategorije
                current_words = cw_datasource.getWordsByCategory(Long.parseLong(view.getTag().toString()));
                //popuni grid sa rijecima
                fillGridWithWords(0);
                view_mode = 1;
            }
            //inace
            else {
                b = (Button)view;
                //upisi rijec u editbox
                if (!b.getText().toString().isEmpty())
                    text.append(" " + b.getText());
                }
            }
    }

    private void fillGridWithWords(int offset){
        for (int i=0; i < gridSlots.size(); i++ ) {
            if (i < current_words.size()) {
                gridSlots.get(i).setText(current_words.get(offset + i).getWord());
                gridSlots.get(i).setTag(current_words.get(offset + i).getId());
            }
            else {
                gridSlots.get(i).setText("");
            }
        }
    }

    private void fillGridWithCategories(int offset){
        for (int i=0; i < categories.size(); i++){
            if (i < categories.size()) {
                gridSlots.get(i).setText(categories.get(offset + i).getName());
                gridSlots.get(i).setTag(categories.get(offset + i).getId());
            }
            else {
                gridSlots.get(i).setText("");
            }
        }
    }
}
