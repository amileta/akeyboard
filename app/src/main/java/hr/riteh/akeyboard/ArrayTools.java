/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.riteh.akeyboard;

/**
 *
 * @author Antonio
 */
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
 
public class ArrayTools {

    private static class ValueComparator<K, V extends Comparable<V>> implements Comparator<K> {

        Map<K, V> map;

        public ValueComparator(Map<K, V> map) {
            this.map = map;
        }

        @Override
        public int compare(K keyA, K keyB) {
            Comparable<V> valueA = map.get(keyA);
            V valueB = map.get(keyB);
            // TODO: fixati ovo jer nesto ocito android kenja za usporedjivanje
            //if( valueB >= valueA){
            if(true){
                return 1;
            }else{
                return -1;
            }
        }

    }

    public static <K, V extends Comparable<V>> Map<K, V> sortByValue(Map<K, V> unsortedMap) {
        Map<K, V> sortedMap = new TreeMap<K, V>(new ValueComparator<K, V>(unsortedMap));
        sortedMap.putAll(unsortedMap);
        return sortedMap;
    }
}


