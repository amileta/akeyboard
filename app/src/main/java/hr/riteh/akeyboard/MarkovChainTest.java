/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.riteh.akeyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 *
 * @author Antonio
 */
public class MarkovChainTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        //TreeMap<String, Double> newmap = (TreeMap) ArrayTools.sortByValue(mapa);
        
        MarkovChain chain = new MarkovChain();
        BufferedReader rd = new BufferedReader(new FileReader(new File("TheTimeMachine.txt")));
        String sentence;
        while((sentence = rd.readLine()) != null){
            StringTokenizer tokenizer = new StringTokenizer(sentence, ",.'\"*#$%&/()[]=?!-+;:>< ");
            String curToken = "";
            String prevToken = "";

            while(tokenizer.hasMoreElements()){
                curToken = tokenizer.nextToken().toLowerCase();
                if(!prevToken.isEmpty()){
                    chain.updateWord(prevToken, curToken);
                }else{
                    chain.updateWord(curToken);
                }
                prevToken = curToken;
            }
        }
       
        
        /*
        chain.updateWord("prva");
        chain.updateWord("druga");
        chain.updateWord("cetvrta");
        chain.updateWord("prva", "treca");
        chain.updateWord("treca", "peta");
        chain.updateWord("prva", "druga");
        chain.updateWord("prva", "druga");
        chain.updateWord("prva", "druga");
        chain.updateWord("prva", "treca");
        chain.updateWord("prva", "treca");
        chain.updateWord("prva", "treca");
        chain.updateWord("prva", "treca");
        chain.updateWord("prva", "prva");
        */
        
        for(Map.Entry entry : chain.getChain().entrySet()){
            System.out.println(entry.getKey() + " - " + entry.getValue());
            //TreeMap<String, Integer> newmap = (TreeMap) ArrayTools.sortByValue((Map)entry.getValue());
            //System.out.println(newmap);
        }
    }
    
    
}
