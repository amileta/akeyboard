/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.riteh.akeyboard;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Antonio
 */
public class MarkovChain implements Serializable {
    HashMap<String,TreeMap> chain;

    public HashMap<String, TreeMap> getChain() {
        return chain;
    }
    
    public MarkovChain() {
        chain = new HashMap<String, TreeMap>();
    }
    
    public void updateWord(String previous, String current){
        // TODO: handlanje null vrijednosti
        updateWord(previous);
        updateWord(current);
        
        TreeMap<String, Integer> prev = chain.get(previous);
        if(prev.containsKey(current)){
            Integer val = prev.get(current);
            prev.put(current, val+1);
        }else{
            prev.put(current, 1);
        }
    }
    
    public void updateWord(String current){
        if(!chain.containsKey(current)){
            chain.put(current, new TreeMap<String, Integer>());
        }
    }
    
    public Set<String> getNextWords(String current){
        if(chain.containsKey(current)){
            TreeMap<String, Integer> map = chain.get(current);
            TreeMap<String, Integer> sortedMap = (TreeMap) ArrayTools.sortByValue((Map)map);
            return sortedMap.keySet();
        }
        return null;
    }
    
    public Set<String> getEntireDictionary(){
        return chain.keySet();
    }
}
