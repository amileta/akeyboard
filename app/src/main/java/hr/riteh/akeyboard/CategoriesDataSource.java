package hr.riteh.akeyboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 17.2.2015..
 */
public class CategoriesDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.CATEGORY_ID,
            MySQLiteHelper.CATEGORY_NAME};

    public CategoriesDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long createCategory(String category) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.CATEGORY_NAME, category);
        long insertId = database.insert(MySQLiteHelper.TABLE_CATEGORIES, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_CATEGORIES,
                allColumns, MySQLiteHelper.CATEGORY_ID + " = '" + insertId + "'", null,
                null, null, null);
        cursor.moveToFirst();
        cursor.close();
        return insertId;
    }

    public void deleteCategory(Category category) {
        long id = category.getId();
        System.out.println("Category deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_CATEGORIES, MySQLiteHelper.CATEGORY_ID
                + " = " + id, null);
    }

    public List<Category> getCategories() {
        //dbHelper.dropDatabase();
        return getCategories(5000, 0);
    }

    public List<Category> getCategories(int limit) {
        return getCategories(limit, 0);
    }

    public List<Category> getCategories(int limit, int offset) {
        List<Category> categories = new ArrayList<Category>();
        String limitClause;
        if (limit !=0 && offset != 0)
            limitClause =  " LIMIT" + limit + " OFFSET " + offset;
        else
            limitClause = null;
            Cursor cursor = database.query(MySQLiteHelper.TABLE_CATEGORIES,
                allColumns, null, null, null, null, MySQLiteHelper.CATEGORY_NAME + " DESC", limitClause );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Category category = cursorToCategory(cursor);
            categories.add(category);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return categories;
    }

    private Category cursorToCategory(Cursor cursor) {
        Category category = new Category();
        category.setId(cursor.getLong(0));
        category.setName(cursor.getString(1));
        return category;
    }
}