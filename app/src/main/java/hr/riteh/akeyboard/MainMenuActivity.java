package hr.riteh.akeyboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Antonio on 18.2.2015..
 */
public class MainMenuActivity extends Activity {


    public MainMenuActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View view){
        switch(view.getId()){
            case R.id.keyboard:
                startActivity(new Intent(this, KeyboardActivity.class));
                break;
            case R.id.pomoc:
                //startActivity(new Intent(this, HelpActivity.class));
                startActivity(new Intent(this, TestDatabaseActivity.class));
                break;
            case R.id.sucelje:
                startActivity(new Intent(this, CustomizeActivity.class));
                break;
        }
    }

}
