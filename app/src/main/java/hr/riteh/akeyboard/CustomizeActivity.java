package hr.riteh.akeyboard;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Antonio on 18.2.2015..
 */
public class CustomizeActivity extends Activity {


    public CustomizeActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spremljene_rijeci);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View view) {

    }
}
