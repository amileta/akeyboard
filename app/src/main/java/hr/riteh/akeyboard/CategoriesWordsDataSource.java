package hr.riteh.akeyboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 17.2.2015..
 */
public class CategoriesWordsDataSource {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.WP_CATEGORY_ID, MySQLiteHelper.WP_POSITION,
            MySQLiteHelper.WP_WORD_ID};
    private final String GET_BY_CATEGORIES_QUERY = "SELECT * FROM " + MySQLiteHelper.TABLE_WORD_POSITIONS
            + " a INNER JOIN " + MySQLiteHelper.TABLE_WORDS + " b ON a."
            + MySQLiteHelper.WP_WORD_ID +"=" + "b."+MySQLiteHelper.CATEGORY_ID
            +" WHERE a." + MySQLiteHelper.WP_WORD_ID+"=?";

    //constructor
    public CategoriesWordsDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }
    public void close() {
        dbHelper.close();
    }

    //adds a word to a category
    public long addWordToCategory(long category_id, int button_id, long word_id) {
        long insertId;
        //check if the word is already added to this category
        /*Cursor cursor = database.query(MySQLiteHelper.TABLE_WORD_POSITIONS,
                allColumns, MySQLiteHelper.KEY_WORD_ID + "= '" + word_id + "' and " + MySQLiteHelper.WP_CATEGORY_ID + "= '" + category_id + "'", null, null, null, null);
        //if it's not added, add it:*/
        //if (!cursor.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.WP_CATEGORY_ID, category_id);
            values.put(MySQLiteHelper.WP_POSITION, button_id);
            values.put(MySQLiteHelper.WP_WORD_ID, word_id);
            //insert into many to many relationship
            insertId = database.insert(MySQLiteHelper.TABLE_WORD_POSITIONS, null, values);
            return insertId;
    }

    public List<Word> getWordsByCategory(long category_id) {
        return getWordsByCategory(category_id, 5000, 0);
    }

    public List<Word> getWordsByCategory(long category_id, int limit) {
        return getWordsByCategory(category_id, limit, 0);
    }

    public List<Word> getWordsByCategory(long category_id, int limit, int offset) {
        List<Word> words = new ArrayList<Word>();

        /*Cursor cursor = database.query(MySQLiteHelper.TABLE_WORD_POSITIONS,
                allColumns, null, null, null, null, null);*/
        Cursor cursor = database.rawQuery(GET_BY_CATEGORIES_QUERY,
                new String[]{String.valueOf(category_id)});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    private Word cursorToWord(Cursor cursor) {
        Word word = new Word();
        word.setId(cursor.getLong(0));
        word.setButtonId(cursor.getInt(1));
        word.setWord(cursor.getString(4));
        return word;
    }
}
